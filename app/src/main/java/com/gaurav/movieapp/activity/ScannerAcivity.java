package com.gaurav.movieapp.activity;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.gaurav.movieapp.Constant;
import com.gaurav.movieapp.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.util.ArrayList;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerAcivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    String movieId;
    Dialog dialog;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner_acivity);

        toolbar= (Toolbar) findViewById(R.id.toolBar);
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        SharedPreferences preferences=getSharedPreferences("MovieId",MODE_PRIVATE);
       movieId= preferences.getString("mid","");
        mScannerView = new ZXingScannerView(this);
        ArrayList<BarcodeFormat> barcodeFormats=new ArrayList<BarcodeFormat>();
        barcodeFormats.add(BarcodeFormat.QR_CODE);
        mScannerView.setFormats(barcodeFormats);
        mScannerView.setLayoutParams(new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        setContentView(mScannerView);// Set the scanner view as the content view
        int i = Camera.getNumberOfCameras();
        Log.e("TAG", "onCreate: " + i);
//        if (i < 2) {
//            ToastMaker.makeToast(this, "You dont have front camera");
//            return;
//        }

        mScannerView.startCamera(Constant.cameraId);
        mScannerView.setResultHandler(this);
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here

        Log.v("TAG", rawResult.getText()); // Prints scan results
        Log.v("TAG", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        if(!movieId.equals("")) {
            if (movieId.equalsIgnoreCase(rawResult.getText().toString())) {
                QrCodeMsgDialog qrCodeExpiredDialog = new QrCodeMsgDialog(ScannerAcivity.this);

                Window window = qrCodeExpiredDialog.getWindow();
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER_HORIZONTAL);
                window.setBackgroundDrawableResource(android.R.color.transparent);
                this.dialog = qrCodeExpiredDialog;
                qrCodeExpiredDialog.show();
                qrCodeExpiredDialog.findViewById(R.id.okBT).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
                qrCodeExpiredDialog.msgTV.setText("Your Tickets Are Booked for Id"+"#"+movieId);

                Toast.makeText(ScannerAcivity.this, "QR Code Scanned", Toast.LENGTH_SHORT).show();
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                Toast.makeText(ScannerAcivity.this, "Your Movie Tickets are Confirmed ", Toast.LENGTH_SHORT).show();
                mScannerView.stopCameraPreview();
                mScannerView.stopCamera();

            } else {
                QrCodeMsgDialog qrCodeExpiredDialog = new QrCodeMsgDialog(ScannerAcivity.this);

                Window window = qrCodeExpiredDialog.getWindow();
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER_HORIZONTAL);
                window.setBackgroundDrawableResource(android.R.color.transparent);
                this.dialog = qrCodeExpiredDialog;
                qrCodeExpiredDialog.show();
                qrCodeExpiredDialog.findViewById(R.id.okBT).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
                qrCodeExpiredDialog.msgTV.setText("Invalid Id! Please Book Your Ticket First");
                mScannerView.stopCameraPreview();
                mScannerView.stopCamera();



                Toast.makeText(ScannerAcivity.this, "QR Code Scanned", Toast.LENGTH_SHORT).show();
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                Toast.makeText(ScannerAcivity.this, "You have not booked the tickets ", Toast.LENGTH_SHORT).show();


            }
        }


    }

    @Override
    public void onBackPressed() {
        mScannerView.stopCameraPreview();
        mScannerView.stopCamera();
        super.onBackPressed();
    }
}

