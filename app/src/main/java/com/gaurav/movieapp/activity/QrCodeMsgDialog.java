package com.gaurav.movieapp.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gaurav.movieapp.R;

public class QrCodeMsgDialog extends Dialog implements View.OnClickListener{
    private Button Ok;
    TextView msgTV;

    public QrCodeMsgDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qrcoderesultmsg);
        setCancelable(false);
        intViews();
    }

    public void intViews() {

        msgTV= (TextView) findViewById(R.id.confirmMsgTV);
        Ok = (Button) findViewById(R.id.okBT);
        Ok.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}