package com.gaurav.movieapp.activity;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gaurav.movieapp.DBHandler;
import com.gaurav.movieapp.model.OfflineMovieDataModel;
import com.gaurav.movieapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by gaurav on 28/5/16.
 */
public class GetMovieDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv_TITLE,tv_RUNTIME,tv_WRITER,tv_LANGUAGE,tv_ACTORS,tv_COUNTRY,tv_DIRECTOR,tv_RELEASED,tv_TYPE,tv_YEAR;
    String imdbId;
    Button bookBT;
    ImageView moviePosterIV ;
    Dialog dialog;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);
        initView();
        addListener();


        DBHandler dbHandler=new DBHandler(this);
        dbHandler.open();
        ArrayList<OfflineMovieDataModel> result=dbHandler.getData();
        dbHandler.close();

        for (int i = 0; i < result.size(); i++) {
            OfflineMovieDataModel offlineMovieDataModel=result.get(i);
            imdbId=offlineMovieDataModel.getImdbID();
            tv_TITLE.setText(offlineMovieDataModel.getTitle());
            tv_RUNTIME.setText(offlineMovieDataModel.getRuntime());
            tv_WRITER.setText(offlineMovieDataModel.getWriter());
            tv_LANGUAGE.setText(offlineMovieDataModel.getLanguage());
            tv_ACTORS.setText(offlineMovieDataModel.getActors());
            tv_COUNTRY.setText(offlineMovieDataModel.getCountry());
            tv_DIRECTOR.setText(offlineMovieDataModel.getDirector());
            tv_RELEASED.setText(offlineMovieDataModel.getReleased());
            tv_TYPE.setText(offlineMovieDataModel.getType());
            tv_YEAR.setText(offlineMovieDataModel.getYear());
            if(offlineMovieDataModel.getPoster().equalsIgnoreCase("N/A")) {
                moviePosterIV.setImageResource(R.drawable.supermanposter);
            }
            else {
                Picasso.with(this).load(offlineMovieDataModel.getPoster()).into(moviePosterIV);
            }


        }




    }

    private void addListener() {
        bookBT.setOnClickListener(this);
    }

    private void initView() {

        tv_TITLE= (TextView) findViewById(R.id.titleTV);
        tv_RUNTIME= (TextView) findViewById(R.id.runtimeTV);
        tv_WRITER= (TextView) findViewById(R.id.writerTV);
        tv_LANGUAGE= (TextView) findViewById(R.id.languageTV);
        tv_ACTORS= (TextView) findViewById(R.id.actorTV);
        tv_COUNTRY= (TextView) findViewById(R.id.countryTV);
        tv_DIRECTOR= (TextView) findViewById(R.id.directorTV);
        tv_RELEASED= (TextView) findViewById(R.id.releasedTV);
        tv_TYPE= (TextView) findViewById(R.id.typeTV);
        tv_YEAR= (TextView) findViewById(R.id.yearTV);
        bookBT= (Button) findViewById(R.id.bookBT);
        moviePosterIV= (ImageView) findViewById(R.id.moviePosterIV);
        toolbar= (Toolbar) findViewById(R.id.toolBar);
//        setSupportActionBar(toolbar);
       toolbar.setTitle("GMOVIE");
        toolbar.setTitleTextColor(Color.WHITE);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });


    }


    @Override
    public void onClick(View v) {

        SharedPreferences preferences=getSharedPreferences("MovieId",MODE_PRIVATE);
        preferences.edit().putString("mid",imdbId).apply();

        QrCodeMsgDialog qrCodeExpiredDialog = new QrCodeMsgDialog(GetMovieDetailsActivity.this);

        Window window = qrCodeExpiredDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL);
        window.setBackgroundDrawableResource(android.R.color.transparent);
        this.dialog = qrCodeExpiredDialog;
        qrCodeExpiredDialog.show();
        qrCodeExpiredDialog.findViewById(R.id.okBT).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        qrCodeExpiredDialog.msgTV.setText("Tickets are Booked and Scan your Ticket from Scanner to get the Confirmation");

    }
}
