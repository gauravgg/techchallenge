package com.gaurav.movieapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.gaurav.movieapp.AppCOntroller;
import com.gaurav.movieapp.DBHandler;
import com.gaurav.movieapp.model.MovieModel;
import com.gaurav.movieapp.R;
import com.google.gson.Gson;

/**
 * Created by ravi on 25/5/16.
 */
public class ListofMovieActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    LinearLayout movieLL1,movieLL2,movieLL3,movieLL4,topdragger;

    String url1= "http://www.omdbapi.com/?t=BAtman&y=2012&plot=full&r=json";
    String url2="http://www.omdbapi.com/?t=Batman%3A+The+Dark+Knight+Returns%2C+Part+2&y=&plot=short&r=json";
    String url3="http://www.omdbapi.com/?t=SuperMan+Returns&y=&plot=short&r=json";
    String url4="http://www.omdbapi.com/?t=SuperMan+&y=2014&plot=short&r=json";
    String imdbId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_screen);
        initViews();
        addListener();



    }

    private void addListener() {
        movieLL1.setOnClickListener(this);
        movieLL2.setOnClickListener(this);
        movieLL3.setOnClickListener(this);
        movieLL4.setOnClickListener(this);
        topdragger.setOnClickListener(this);
    }

    private void initViews() {
        movieLL1= (LinearLayout) findViewById(R.id.movieLL1);
        movieLL2= (LinearLayout) findViewById(R.id.movieLL2);
        movieLL3= (LinearLayout) findViewById(R.id.movieLL3);
        movieLL4= (LinearLayout) findViewById(R.id.movieLL4);
        topdragger= (LinearLayout) findViewById(R.id.topDragger);
        toolbar= (Toolbar) findViewById(R.id.toolBar);
//        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setTitle("GMOVIE");
        toolbar.setTitleTextColor(Color.WHITE);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               finish();
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);


            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.movieLL1:

             getMovieDetailsRequest(url1);

                break;
            case R.id.movieLL2:

                getMovieDetailsRequest(url2);

                break;
            case R.id.movieLL3:
                getMovieDetailsRequest(url3);

                break;
            case R.id.movieLL4:
                getMovieDetailsRequest(url4);

                break;
            case R.id.topDragger:
                Intent intent=new Intent(this,ScannerAcivity.class);
                startActivity(intent);

                break;
        }
    }

 private  void  getMovieDetailsRequest(String url){

     StringRequest request=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
         @Override
         public void onResponse(String response) {
             Gson gson=new Gson();
             MovieModel movieModel=gson.fromJson(response,MovieModel.class);

             if (movieModel.getResponse().equalsIgnoreCase("True")){

                 Toast.makeText(ListofMovieActivity.this, "Response Got It Yeaaaahhhh..", Toast.LENGTH_SHORT).show();
                 DBHandler dbh=new DBHandler(ListofMovieActivity.this);
                 dbh.open();
                 dbh.write(movieModel);
                 dbh.close();

                 Intent intent=new Intent(ListofMovieActivity.this,GetMovieDetailsActivity.class);
                 startActivity(intent);
             }
             else if(response.toString().equals("")){
                 Intent intent=new Intent(ListofMovieActivity.this,GetMovieDetailsActivity.class);
                 startActivity(intent);


             }



         }
     }, new Response.ErrorListener() {
         @Override
         public void onErrorResponse(VolleyError error) {

         }
     });

     request.setRetryPolicy(new DefaultRetryPolicy(100000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
     AppCOntroller.getsInstance().getRequestQueue().add(request);



 }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences preferences=getSharedPreferences("QRCODE", MODE_PRIVATE);
       imdbId= preferences.getString("Qr", "");


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);

    }
}
