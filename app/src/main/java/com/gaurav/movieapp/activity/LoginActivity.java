package com.gaurav.movieapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.gaurav.movieapp.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    EditText enterUserIdET;
    EditText enterPinET;
    Button enterBT;
    SharedPreferences preferences;
    String username,password;
    String uname,pwd;
    boolean aBoolean=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences=getSharedPreferences("MovieAPP", MODE_PRIVATE);
      uname=  preferences.getString("user_id","");
      pwd=  preferences.getString("password","");
        if(uname==""&&pwd==""){
            setContentView(R.layout.activity_login);

            initViews();
            addListener();

        }


        else{

            Intent intent = new Intent(LoginActivity.this, ListofMovieActivity.class);
            startActivity(intent);
        }


    }

    public void initViews() {
        enterUserIdET = (EditText) findViewById(R.id.enterUserIdET);
        enterPinET = (EditText) findViewById(R.id.enterPinET);
        enterBT = (Button) findViewById(R.id.enterBT);
          }

    public void addListener() {
        enterBT.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

     username=enterUserIdET.getText().toString();
     password=enterPinET.getText().toString();


        if(username.equalsIgnoreCase("ABC")&&password.equalsIgnoreCase("ABC")){
        SharedPreferences preferences=getSharedPreferences("MovieAPP", MODE_PRIVATE);
            SharedPreferences.Editor editor=preferences.edit();
            editor.putString("user_id", username);
            editor.putString("password", password);

            editor.apply();
            Intent intent=new Intent(LoginActivity.this,ListofMovieActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();

        }

    }
}