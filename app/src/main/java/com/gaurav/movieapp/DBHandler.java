package com.gaurav.movieapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.gaurav.movieapp.model.MovieModel;
import com.gaurav.movieapp.model.OfflineMovieDataModel;

import java.util.ArrayList;

/**
 * Created by gaurav on 28/5/16.
 */
public class DBHandler {

    public static final String DB_NAME="MovieDatabase.db";
    public static final int DB_VER=2;

    public static final String DB_TABLE="MyMovieTable";
    public static final String C_RELEASED= "Released";
    public static final String C_TYPE="Type";
    public static  final String C_RUNTIME="Runtime";
    public static final String C_TITLE="Title";
    public static final  String C_YEAR="Year";
    public static final  String C_ACTORS="Actors";
    public static final  String C_WRITER="Writer";
    public static final String C_LANGUAGE="Language";
    public static final  String C_DIRECTOR="Director";
    public static final  String C_COUNTRY="Country";
    public static final String C_IMDBID="imdbID";
//    public static final String C_IMDBVOTES="imdbVotes";
//    public static final String C_RESPONSE="Response";
      public static final String C_POSTER="Poster";

//    public static final  String C_IMDBRATING="imdbRating";
//    public static final  String C_RATED="Rated";
//    public static final  String C_PLOT="Plot";
//    public static final  String C_METASCORE="Metascore";
//    public static final  String C_GENRE="Genre";
//    public static final  String C_AWARDS="Awards";


    public static   final String CREATE_QUERY="CREATE TABLE "+DB_TABLE + "(" + C_IMDBID +" TEXT, " + C_TITLE +" TEXT, "
        +C_RELEASED+" TEXT, "+C_YEAR+" TEXT, "
        +C_ACTORS+" TEXT, "+C_DIRECTOR+" TEXT, "
        +C_COUNTRY+" TEXT, "+C_LANGUAGE+" TEXT, "
        +C_WRITER+" TEXT, "+C_RUNTIME+" TEXT, "
            +C_POSTER+" TEXT, "+C_TYPE+" TEXT);";

    public static final String DROP_QUERY="DROP TABLE "+ DB_TABLE +";";




    private class DBInternal extends SQLiteOpenHelper{

        public DBInternal(Context context) {
            super(context, DB_NAME, null, DB_VER);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_QUERY);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DROP_QUERY);
            onCreate(db);

        }
    }

DBInternal dbInternal;
    Context ourcontext;
    SQLiteDatabase database;
    public DBHandler(Context context) {
        this.ourcontext=context;

    }

    public DBHandler open() {

        dbInternal=new DBInternal(ourcontext);
        database=dbInternal.getWritableDatabase();
        return this;

    }

    public void write(MovieModel movieModel) {
        ContentValues contentValues=new ContentValues();
        contentValues.put(C_IMDBID,movieModel.getImdbID());
        contentValues.put(C_ACTORS,movieModel.getActors());
        contentValues.put(C_DIRECTOR,movieModel.getDirector());
        contentValues.put(C_COUNTRY,movieModel.getCountry());
        contentValues.put(C_RELEASED,movieModel.getReleased());
        contentValues.put(C_LANGUAGE,movieModel.getLanguage());
        contentValues.put(C_RUNTIME,movieModel.getRuntime());
        contentValues.put(C_WRITER,movieModel.getWriter());
        contentValues.put(C_TYPE,movieModel.getType());
        contentValues.put(C_TITLE,movieModel.getTitle());
        contentValues.put(C_YEAR,movieModel.getYear());
        contentValues.put(C_POSTER,movieModel.getPoster());

        database.insert(DB_TABLE, null, contentValues);
        Toast.makeText(ourcontext, "Yeahhhh Data stored in database", Toast.LENGTH_SHORT).show();

    }

    public void close() {
        database.close();

    }
    public ArrayList<OfflineMovieDataModel> getData() {

        ArrayList<OfflineMovieDataModel> strings=new ArrayList<>();
        String columns[]={C_IMDBID,C_TITLE,C_RUNTIME,C_WRITER,C_LANGUAGE,C_ACTORS,C_COUNTRY,C_DIRECTOR,C_RELEASED,C_TYPE,C_YEAR,C_POSTER};

        Cursor cursor=database.query(DB_TABLE,columns,null,null,null,null,null);
        int i_POSTER=cursor.getColumnIndex(C_POSTER);
        int i_IMDBID=cursor.getColumnIndex(C_IMDBID);
        int i_TITLE=cursor.getColumnIndex(C_TITLE);
        int    i_RUNTIME=cursor.getColumnIndex(C_RUNTIME);
        int     i_WRITER=cursor.getColumnIndex(C_WRITER);
        int     i_LANGUAGE=cursor.getColumnIndex(C_LANGUAGE);
        int     i_ACTORS=cursor.getColumnIndex(C_ACTORS);
        int     i_COUNTRY=cursor.getColumnIndex(C_COUNTRY);
        int     i_DIRECTOR=cursor.getColumnIndex(C_DIRECTOR);
        int     i_RELEASED=cursor.getColumnIndex(C_RELEASED);
        int     i_TYPE=cursor.getColumnIndex(C_TYPE);
        int     i_YEAR=cursor.getColumnIndex(C_YEAR);
        for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
            OfflineMovieDataModel offlineMovieDataModel=new OfflineMovieDataModel();
            offlineMovieDataModel.setImdbID(cursor.getString(i_IMDBID));
            offlineMovieDataModel.setTitle(cursor.getString(i_TITLE));
            offlineMovieDataModel.setRuntime(cursor.getString(i_RUNTIME));
            offlineMovieDataModel.setActors(cursor.getString(i_ACTORS));
            offlineMovieDataModel.setCountry(cursor.getString(i_COUNTRY));
            offlineMovieDataModel.setDirector(cursor.getString(i_DIRECTOR));
            offlineMovieDataModel.setLanguage(cursor.getString(i_LANGUAGE));
            offlineMovieDataModel.setReleased(cursor.getString(i_RELEASED));
            offlineMovieDataModel.setType(cursor.getString(i_TYPE));
            offlineMovieDataModel.setYear(cursor.getString(i_YEAR));
            offlineMovieDataModel.setWriter(cursor.getString(i_WRITER));
            offlineMovieDataModel.setPoster(cursor.getString(i_POSTER));
            strings.add(offlineMovieDataModel);

        }

        return strings;

    }
}
